package main

import (
	"database/sql"
	"log"

	"github.com/gin-gonic/gin"
	_ "github.com/mattn/go-sqlite3"
)

// POST /graph/<id>/shortestpath {"from": v, "to": w } => [ v, ..., w ]
// POST /graph/<id>/connected {"node1": v, "node2": w } => true | false
func main() {
	log.Print("connecting to db: start")
	db, err := sql.Open("sqlite3", "sqlite.db")
	if err != nil {
		log.Fatal(err)
	}
	log.Print("connecting to db: finish")
	graphModelMigrate(db)

	router := gin.Default()

	graphApi := router.Group("/graph")
	graphApi.PUT("", func(c *gin.Context) {
		graphCreate(db, c)
	})
	graphApi.PUT("/:gid/edge", func(c *gin.Context) {
		graphAddEdge(db, c)
	})
	graphApi.GET("/:gid/nodes", func(c *gin.Context) {
		graphGetNodes(db, c)
	})
	graphApi.GET("/:gid/node/:nid/neighbors", func(c *gin.Context) {
		graphGetNodeNeighbors(db, c)
	})

	analyticsApi := router.Group("/analytics")
	analyticsApi.POST("/:gid/shortestpath", func(c *gin.Context) {
		analyticsShortestPath(db, c)
	})
	analyticsApi.POST("/:gid/connected", func(c *gin.Context) {
		analyticsConnected(db, c)
	})
	analyticsApi.POST("/:gid/mst", func(c *gin.Context) {
		analyticsMst(db, c)
	})

	router.Run()
}
