package main

import (
	"database/sql"
	"errors"
	"log"

	"github.com/yourbasic/graph"
)

func graphModelMigrate(db *sql.DB) {
	stmt := `
	create table if not exists graph(
		id integer primary key autoincrement,
		g_order integer
	);
	`
	log.Print("creating graph table if missing: start")
	if _, err := db.Exec(stmt); err != nil {
		log.Fatal(err)
	}
	log.Print("creating graph table if missing: finish")
	stmt2 := `
	create table if not exists edge(
		gid integer,
		n1 integer,
		n2 integer
	)`
	log.Print("creating edge table if missing: start")
	if _, err := db.Exec(stmt2); err != nil {
		log.Fatal(err)
	}
	log.Print("creating edge table if missing: finish")
}

func graphModelCreate(db *sql.DB, order int) int64 {
	stmt := `
		insert into graph (g_order) values (?);
		`
	log.Print("inserting graph row: start")
	res, err := db.Exec(stmt, order)
	if err != nil {
		log.Panic(err)
	}

	id, err := res.LastInsertId()
	if err != nil {
		log.Panic(err)
	}
	log.Print("inserting graph row: finish")
	return id
}

func graphModelAddEdge(db *sql.DB, gid int, node1 int, node2 int) {
	stmt := `
	insert into edge (gid, n1, n2) values (?,?,?);
	`
	log.Print("inserting edge row: start")
	_, err := db.Exec(stmt, gid, node1, node2)
	if err != nil {
		log.Panic(err)
	}
	log.Print("inserting edge row: finish")
}

func graphGetById(db *sql.DB, gid int) (graph.Iterator, error) {
	log.Print("fetch graph: start")

	row := db.QueryRow("SELECT * FROM graph where id = ?", gid)
	var graphRow GraphRow
	if err := row.Scan(&graphRow.Id, &graphRow.Order); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, err
		}
		return nil, err
	}
	log.Print("fetch graph: finish")

	log.Print("fetch edges: start")
	rows, err := db.Query("SELECT n1, n2 FROM edge WHERE gid = ?", gid)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var all []EdgeRow
	for rows.Next() {
		var edgeRow EdgeRow
		if err := rows.Scan(&edgeRow.Node1, &edgeRow.Node2); err != nil {
			return nil, err
		}
		all = append(all, edgeRow)
	}
	log.Print("fetch edges: finish")

	g := graph.New(graphRow.Order)
	for _, e := range all {
		g.AddBoth(e.Node1, e.Node2)
	}
	return g, nil
}

type GraphRow struct {
	Id    int
	Order int
}

type EdgeRow struct {
	Node1 int
	Node2 int
}

func graphModelNodes(g graph.Iterator) []int {
	nodes := make([]int, 0)
	for i := 0; i < g.Order(); i++ {
		nodes = append(nodes, i)
	}
	return nodes
}

func graphModelNodeNeighbors(g graph.Iterator, node int) []int {
	nbrs := make([]int, 0)
	g.Visit(node, func(w int, c int64) bool {
		nbrs = append(nbrs, w)
		return false
	})
	return nbrs
}

func graphModelIsConnected(g graph.Iterator, n1 int, n2 int) bool {
	cnctd := false
	graph.BFS(g, n1, func(_ int, to int, _ int64) {
		if to == n2 {
			cnctd = true
		}
	})
	return cnctd
}
