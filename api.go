package main

import (
	"database/sql"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/yourbasic/graph"
)

func graphCreate(db *sql.DB, c *gin.Context) {
	var putGraphData PutGraphData
	log.Print("parsing body: start")
	if err := c.BindJSON(&putGraphData); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	log.Print("parsing body: finish")
	id := graphModelCreate(db, putGraphData.Order)
	jsonOK(c, gin.H{"graphId": id})
}

func graphAddEdge(db *sql.DB, c *gin.Context) {
	log.Print("parsing params: start")
	gid2, err := getIntParam(c, "gid")
	if err != nil {
		log.Panic(err)
	}
	log.Print("parsing params: finish")
	var putEdgeData PutEdgeData
	log.Print("parsing body: start")
	if err := c.BindJSON(&putEdgeData); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	log.Print("parsing body: finish")
	graphModelAddEdge(db, gid2, putEdgeData.Node1, putEdgeData.Node2)
	stringOK(c, "")
}

func graphGetNodes(db *sql.DB, c *gin.Context) {
	log.Print("parsing params: start")
	gid2, err := getIntParam(c, "gid")
	if err != nil {
		log.Panic(err)
	}
	log.Print("parsing params: finish")
	g, err := graphGetById(db, gid2)
	if err != nil {
		log.Panic(err)
	}
	jsonOK(c, graphModelNodes(g))
}

func graphGetNodeNeighbors(db *sql.DB, c *gin.Context) {
	log.Print("parsing params: start")
	gid2, err := getIntParam(c, "gid")
	if err != nil {
		log.Panic(err)
	}
	nid2, err := getIntParam(c, "nid")
	if err != nil {
		log.Panic(err)
	}
	log.Print("parsing params: finish")
	g, err := graphGetById(db, gid2)
	if err != nil {
		log.Fatal(err)
	}
	jsonOK(c, graphModelNodeNeighbors(g, nid2))
}

func analyticsShortestPath(db *sql.DB, c *gin.Context) {
	log.Print("parsing params: start")
	gid2, err := getIntParam(c, "gid")
	if err != nil {
		log.Panic(err)
	}
	log.Print("parsing params: finish")
	var shortestPathData ShortestPathData
	log.Print("parsing body: start")
	if err := c.BindJSON(&shortestPathData); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	log.Print("parsing body: end")

	g, err := graphGetById(db, gid2)
	if err != nil {
		log.Fatal(err)
	}
	path, _ := graph.ShortestPath(g, shortestPathData.From, shortestPathData.To)
	jsonOK(c, path)
}

func analyticsConnected(db *sql.DB, c *gin.Context) {
	log.Print("parsing params: start")
	gid2, err := getIntParam(c, "gid")
	if err != nil {
		log.Panic(err)
	}
	log.Print("parsing params: finish")
	var connectedData ConnectedData
	log.Print("parsing body: start")
	if err := c.BindJSON(&connectedData); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	log.Print("parsing body: end")

	g, err := graphGetById(db, gid2)
	if err != nil {
		log.Fatal(err)
	}
	cnctd := graphModelIsConnected(g, connectedData.Node1, connectedData.Node2)
	jsonOK(c, cnctd)
}

func analyticsMst(db *sql.DB, c *gin.Context) {
	log.Print("parsing params: start")
	gid2, err := getIntParam(c, "gid")
	if err != nil {
		log.Panic(err)
	}
	log.Print("parsing params: finish")

	g, err := graphGetById(db, gid2)
	if err != nil {
		log.Fatal(err)
	}
	jsonOK(c, graph.MST(g))
}

type PutGraphData struct {
	Order int `json:"order" binding:"required"`
}

// how to set binding required when value is 0
type PutEdgeData struct {
	Node1 int `json:"node1"`
	Node2 int `json:"node2"`
}

type ShortestPathData struct {
	From int `json:"from"`
	To   int `json:"to"`
}

type ConnectedData struct {
	Node1 int `json:"node1"`
	Node2 int `json:"node2"`
}

func getIntParam(c *gin.Context, name string) (int, error) {
	v := c.Param(name)
	return strconv.Atoi(v)
}

func stringOK(c *gin.Context, s string) {
	c.String(http.StatusOK, s)
}

func jsonOK(c *gin.Context, body interface{}) {
	c.JSON(http.StatusOK, body)
}
